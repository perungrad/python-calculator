import re

class Calculator:
    numberRegex = "-?\d+(?:\.\d+)*"
    lineRegex = ""

    def __init__(self):
        self.lineRegex = "(" + self.numberRegex + "|\+|-|\*|/|\(|\))"

    def checkInputValidity(self, input):
        items = re.findall(self.lineRegex, input)

        return self.checkValidity(items)

    def execute(self, input):
        items = re.findall(self.lineRegex, input)

        if not self.checkValidity(items):
            return False

        items = self.convertStringsToFloat(items)

        return self.calculate(items)[0]

    def checkValidity(self, inputArr):
        allowedAfterSymbol = {
            'start':  ['number', '('],
            'number': ['+', '-', '*', '/', ')', 'end'],
            '+':      ['number', '('],
            '-':      ['number', '('],
            '*':      ['number', '('],
            '/':      ['number', '('],
            '(':      ['number', '('],
            ')':      [')', '+', '-', '*', '/', 'end'],
        }

        numOfOpeningParenthesis = 0
        numOfClosingParenthesis = 0

        previousSymbol = 'start'

        for item in inputArr:
            symbol = item

            if re.match('^' + self.numberRegex + '$', symbol):
                symbol ='number'

            if not symbol in allowedAfterSymbol[previousSymbol]:
                return False

            if symbol == '(':
                numOfOpeningParenthesis += 1

            if symbol == ')':
                numOfClosingParenthesis += 1

            if numOfClosingParenthesis > numOfOpeningParenthesis:
                return False

            previousSymbol = symbol

        if not 'end' in allowedAfterSymbol[previousSymbol]:
            return False

        if numOfOpeningParenthesis != numOfClosingParenthesis:
            return False

        return True

    def convertStringsToFloat(self, items):
        newItems = []

        for item in items:
            newItem = item

            if re.match('^' + self.numberRegex + '$', item):
                newItem = float(item)

            newItems.append(newItem)

        return newItems

    def calculate(self, items):
        while '(' in items:
            openingIdx = items.index('(')

            closingIdx = -1
            numOfInsideParenhesis = 0

            for idx, item in enumerate(items[openingIdx + 1:]):
                if item == '(':
                    numOfInsideParenhesis += 1
                if item == ')':
                    if numOfInsideParenhesis == 0:
                        closingIdx = idx + openingIdx + 1
                        break
                    else:
                        numOfInsideParenhesis -= 1

            if closingIdx == -1:
                raise Exception('Invalid parenthesis')

            insideParenthesis = self.calculate(items[openingIdx + 1 : closingIdx])

            items = items[:openingIdx] + insideParenthesis + items[closingIdx + 1:]

        items = self.calculateOperation(items, '*')
        items = self.calculateOperation(items, '/')
        items = self.calculateOperation(items, '+')
        items = self.calculateOperation(items, '-')

        return items

    def calculateOperation(self, items, operator):
        while True:
            if operator in items:
                idx = items.index(operator)

                result = self.getResult(items[idx - 1], items[idx + 1], operator)

                items = items[:max(idx - 1, 0)] + [result] + items[idx + 2:]
            else:
                break;

        return items

    def getResult(self, leftOperand, rightOperand, operator):
        if operator == '*':
            return leftOperand * rightOperand

        if operator == '/':
            return leftOperand / rightOperand

        if operator == '+':
            return leftOperand + rightOperand

        if operator == '-':
            return leftOperand - rightOperand

        return 0

