from calculator import Calculator
from sys import stdin

calculator = Calculator()

#print(calculator.execute("55 + 4 * (1 + 2 - (3 + 8 / 2))"))
#print(calculator.execute("-55 + 4 * (1 + 2 - (3 + 8 / 2))"))
#print(calculator.execute("3 + 33.12345678"))
#print(calculator.execute("(100.57)"))
#print(calculator.execute("-1"))

prompt = 'Enter formula or press CTRL+D to end'

print(prompt)

for line in stdin:
    if calculator.checkInputValidity(line):
        print('=>', calculator.execute(line))
    else:
        print('Invalid input')

    print("")
    print(prompt)

